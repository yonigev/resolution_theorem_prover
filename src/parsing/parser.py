from syntax.operator import  *
from syntax.term import *
from syntax.quantifier import *
from syntax.sentence import *
from syntax.predicate import Predicate
from syntax.cnf import *
from syntax.equality import Equality, Leq
import re
class FOLParser(object):
    def __init__(self):
        self.constants = []
        self.variables = []
        self.functions = []
        self.equalities = []

    def parse_predicate(self, prefix_is_not, pred):
        # skip "not "
        if prefix_is_not:
            pred = pred[3:].lstrip()

        string_rep = str(pred).lstrip()
        name = ''
        if str(pred[0]).upper() != str(pred[0]):
            raise BaseException('predicate {} does not start with uppercase'.format(pred))
        (name, rest) = self.append_chars_until(s=pred, delimiter='(')

        if not (str(rest).startswith('(') and str(rest).endswith(')')):
            raise BaseException('predicate arguments {} not in parentheses'.format(rest))
        rest = rest.rstrip()
        rest = rest.lstrip()
        rest = self.balance_paren(s=rest)
        args_string = rest[1:-1].replace(' ', '')
        parsed_args = self.parse_predicate_args(args_string)
        return Predicate(name=self.balance_paren(name), args=parsed_args, value=(not prefix_is_not))
    def balance_paren(self, s):
        open = str(s).count('(')
        close = str(s).count(')')
        if open == close:
            return s
        if open < close:
            new_s = s[:str(s).rfind(')')]
        else:
            new_s = s[str(s).find('(')+1:]
        return new_s
    # parse a function
    def parse_function(self, func):
        if str(func)[0] != str(func).lower()[0]:
            raise BaseException('not a function')
        (func_name, func_args) = self.append_chars_until(s=func, delimiter='(')
        func_args = func_args.replace('(', '')
        func_args = func_args.replace(')', '')
        func_args = func_args.split(',')
        f = Function(name=func_name, params=func_args)
        return f

    def isolate_arg(self, args_string):
        first_paren = str(args_string).find('(')
        first_comma = str(args_string).find(',')
        # if next arg has arguments
        if first_paren < first_comma:
            (full_arg, rest) = self.append_chars_until(args_string, ')', including_last=True)
            return full_arg
        else:
            (arg, rest) = self.append_chars_until(args_string, ',')
            return arg

    # parse args for a predicate
    def parse_predicate_args(self, args_string):
        parsed = []
        # if complex args
        while args_string != '':
            arg = self.isolate_arg(args_string=args_string)
            if '(' in args_string and ')' in args_string:
                # if inner predicate
                if str(arg[0]).isupper() and str(arg[0]).isalpha():
                    prefix_is_not = True if str(arg).startswith('not') else False
                    parsed_arg = self.parse_predicate(pred=arg, prefix_is_not=prefix_is_not)
                    special_arg = parsed_arg
                elif str(arg[0]).islower() and '(' not in arg:
                    parsed_arg = Variable(arg)
                    self.variables.append(parsed_arg)
                    special_arg = parsed_arg
                elif str(arg[0]).islower() or '(' in arg:
                    parsed_arg = self.parse_function(func=arg)
                    self.functions.append(parsed_arg)
                    special_arg = parsed_arg
                # if inner Constant
                else:# if not str(arg[0]).isalpha() or str(arg[0]).isupper():
                    parsed_arg = Constant(arg)
                    self.constants.append(parsed_arg)
                    special_arg = parsed_arg

                args_string = args_string[len(special_arg.string_rep()):]
                # in case a comma was left when skipping special argument
                if len(args_string) > 0 and args_string[0] == ',':
                    args_string = args_string[1:]
            else:
                (arg, rest) = self.append_chars_until(s=args_string, delimiter=',')
                if (str(arg[0]).isupper() and arg[0].isalpha()) or (not str(arg[0]).isalpha()):
                    # if no parentheses, treat as a constant
                    parsed_arg = Constant(arg)
                    self.constants.append(parsed_arg)
                elif not ('(' in arg or ')' in arg) and arg[0].isalpha():
                    parsed_arg = Variable(arg)
                    self.variables.append(parsed_arg)
                else:
                    parsed_arg = self.parse_function(func=arg)
                    self.functions.append(parsed_arg)
                args_string = self.skip_next_arg(args_string)
            parsed.append(parsed_arg)


        return parsed

    def parse_clause_or_axiom(self, s):
        if '=' in s:
            return self.parse_equality(s)
        else:
            return self.parse_clause(s)


    def parse_clause(self, s):
        string_rep = str(s)
        try:
            clause_predicates = []
            while s:
                if str(s).startswith('not '):
                    prefix_is_not = True
                else:
                    prefix_is_not = False
                # if there's an or
                pred_string = self.isolate_clause(s)
                if '=' in pred_string:
                    pred = self.parse_equality(pred_string)
                else:
                    pred = self.parse_predicate(prefix_is_not=prefix_is_not, pred=pred_string)
                clause_predicates.append(pred)
                s = self.skip_next_term(s)
            parsed_clause = Clause(terms=clause_predicates)
            return parsed_clause

        except BaseException as e:
            print('crap, should not be here {}'.format(e))
            raise e

    def parse_equality(self, string_rep):
        if '=' in string_rep:
            args = string_rep.split('=')
            lhs = args[0]
            rhs = args[1]
            eq = Equality(rhs=rhs, lhs=lhs,string_rep=string_rep)
            self.equalities.append(eq)
            return eq

    def parse_leq(self, string_rep):
        if 'leq(' in string_rep:
            args = string_rep.replace('leq(', '').replace(')', '').replace(' ', '').split(',')
            leq = Leq(lhs=args[0], rhs=args[1], string_rep=string_rep)
            self.equalities.append(leq)
            return leq

    @staticmethod
    def isolate_clause(st):
        try:
            new_s = st[:st.index(' or ')]
            # if not (end of string)
        except ValueError as e:
            new_s = str(st)
        return new_s

    @staticmethod
    def skip_next_term(st):
        try:
            new_s = st[st.index(' or ') + 4:]
            # if not (end of string)
        except ValueError as e:
            new_s = ''
        return new_s


    @staticmethod
    def skip_next_arg(st):
        try:
            new_s = st[st.index(',') + 1:]
            # if not (end of string)
        except ValueError as e:
            new_s = ''
        return new_s

    # return the extracted string and the rest
    def append_chars_until(self, s, delimiter, including_last=False):
        # if closing parentheses needed
        paren_count = 0
        balance_paren = True if delimiter == ')' else False
        if s == '':
            return s
        c = s[0]
        acc= ''
        delimiter_reached = False
        while not delimiter_reached or (paren_count > 0 and balance_paren):

            if c == '(':
                paren_count += 1
            elif c == ')':
                paren_count -= 1
            acc += c
            s = s[1:]
            if s == '':
                break
            c = s[0]
            if c == delimiter:
                delimiter_reached = True
        # if including_last:
        #     return acc + c, s
        return acc, s

    # remove the beginning of the string of length 'length'
    def advance(self, length):
        self.to_parse = self.to_parse[length:]

if __name__ == '__main__':
    s = 'not Time(t, S) and not Loc(v1, s) or not Edge(e, v1, v2) or not Weight(e, w) or not Deadline(d) or not leq(+(w, t), d) or  OK(e, v1, v2, s)'
    parser = FOLParser()
    clause = parser.parse_clause(s=s)
    print('done?')

