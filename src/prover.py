import argparse
from parsing.parser import FOLParser
from syntax.cnf import Clause
import os
#Safe(x,Result(traverse(E2),Result(traverse(E3),Result(traverse(E3),Result(traverse(E2),S0))))) and leq(0, x) and not x=0
import time
class ResolutionProver(object):
    def __init__(self, kb_file, script=False):
        self.script_file = script
        self.kb_file = kb_file
        self.parser = FOLParser()
        (clauses, equalities) = self.parse_kb()

        self.clauses = clauses
        self.equalities = equalities
        self.assumption = None

    # just to make the lists look better. although Leq is a predicate
    def split_clauses_eqs(self):
        for clause in self.clauses:
            # if ("leq" in clause.string_rep().lower() or 'Eq' in clause.string_rep()) and len(clause.terms) == 1:
            if len(clause.terms) == 1:
                if clause.terms[0].name == 'Eq' or clause.terms[0].name == 'Leq':
                    self.equalities.append(clause)
                    #self.clauses.remove(clause)
        self.clauses = filter(lambda clause: clause not in self.equalities, self.clauses)



    def parse_kb(self):
        clauses = []
        equalities = []
        print(os.getcwd())
        with open(self.kb_file) as kb:
            lines = kb.readlines()
            for line in lines:
                # ignore LINE comments
                if line[0] == '#' or len(line) == 0:
                    continue
                if '=' in line:
                    equalities.append(self.parser.parse_equality(line))
                    continue
                line = line.rstrip()
                if line.replace(' ','') == '':
                    continue
                clauses_str_list = line.split('and')
                # add this line's clauses to the KB
                map(lambda s: clauses.append(self.parser.parse_clause(s)), clauses_str_list)
        return clauses, equalities

    def resolve(self, literal, clause, unifier):
        literal,clause = self.process_resolve_input(literal, clause)
        if isinstance(literal, Clause) and isinstance(clause,Clause):
            if len(literal.terms) > 1 and len(clause.terms) > 1:
                raise BaseException('both argument for resolve have more than 1 term!:\n{}\n{}'
                                    .format(literal.string_rep(), clause.string_rep()))
            (literal,clause) = (clause.terms[0], literal) if len(clause.terms) == 1 else (literal.terms[0], clause)
        if isinstance(literal, str):
            literal = self.parser.parse_predicate(prefix_is_not=str(literal).startswith("not"), pred=literal)
        if isinstance(clause, int):
            clause = self.clause_for_num(clause)
        if isinstance(literal, int):
            literal = self.clause_for_num(literal)


        return clause.resolve(literal=literal, unifier=unifier)

    def resolve_with_eq_axiom(self, to_resolve, eq):
        if isinstance(to_resolve, int):
            to_resolve = self.clause_for_num(to_resolve)
        elif isinstance(to_resolve, str):
            to_resolve = self.parser.parse_clause(to_resolve)
        if isinstance(eq, int):
            eq = self.equality_for_num(eq)
            eq = eq.terms[0] if isinstance(eq, Clause) else eq
        return to_resolve.resolve_with_eq_axiom(eq)

    def get_clause_for_string_rep(self, string_rep):
        for clause in self.clauses:
            if clause.string_rep == string_rep:
                return clause

    def print_clauses(self):
        s = ''
        index = 1
        for c in self.clauses:
            s += '[{}]'.format(index) +' {}\n'.format(c.string_rep())
            index += 1
        print(s)

    def print_equality_axioms(self):
        s = ''
        index = 1
        for c in self.equalities:
            s += '[{}]'.format(index) +' {}\n'.format(c.string_rep())
            index += 1
        print(s)

    # to start at 1 for cosmetic reasons
    def clause_for_num(self, num):
        return self.clauses[num - 1]

    # to start at 1 for cosmetic reasons
    def equality_for_num(self, num):
        return self.equalities[num - 1]

    def query_false_assumption(self):
        to_prove = raw_input('Please enter what youre trying to contradict:')
        opposite_pred = self.parser.parse_clause(to_prove)
        self.assumption = opposite_pred

    # return one LITERAL and one CLAUSE for resolving
    def process_resolve_input(self, input1, input2):
        parsed_1 = self.clause_for_num(input1) if isinstance(input1, int) \
                        else input1
        parsed_2 = self.clause_for_num(input2) if isinstance(input2, int) \
                        else input2

        if (not isinstance(parsed_1, str) and not isinstance(parsed_2, str))\
                and (len(parsed_1.terms) > 1 and len(parsed_2.terms) > 1):
            raise BaseException('Both inputs are complex! 1 needs to be a predicate')
        if isinstance(input1, str):
            return input1, parsed_2
        if isinstance(input2, str):
            return input2, parsed_1
        (literal, complex) = (parsed_1, parsed_2) if len(parsed_1.terms) == 1 else (parsed_2, parsed_1)

        return literal, complex

    def process_unifier(self, unifier_input):

        subs = unifier_input.split(',')
        # for sub in subs:
        #     var = sub.split(':')[0].rstrip().lstrip()
        #     val = sub.split(':')[1].rstrip().lstrip()
        #     s += "\'{}\' : \'{}\'".format(str(var), str(val))
        #     s += " "
        # s = "{" + s + "}"
        return Unifier(unifier_input)

    def query_resolve(self):
        to_resolve = raw_input('resolve: (string or index) ').replace('  ', ' ')
        if not to_resolve[0].isalpha():
            to_resolve = int(to_resolve)
        resolve_with = raw_input('with: (string or index) ').replace('  ', ' ')
        if resolve_with.startswith('eq'):
            eq_num = int(resolve_with.replace('eq ', '').replace(' ', ''))
            eq = self.equality_for_num(eq_num)
            _complex = self.clause_for_num(int(to_resolve))

            return eq, _complex, 'eq'

        else:
            if not resolve_with[0].isalpha():
                resolve_with = int(resolve_with)
            (_literal, _complex) = self.process_resolve_input(to_resolve, resolve_with)
            unifier_s = raw_input('unifier ( var1|val1, var2|val2, etc...)')
            unifier = self.process_unifier(unifier_s)
            print('la')
            return _literal, _complex, unifier


    # main prover function
    def main(self, from_script=False):
        self.split_clauses_eqs()

        self.print_equality_axioms()
        self.print_clauses()
        if not from_script:
            self.query_false_assumption()
        print '-'*100
        if from_script:
            self.run_script()
        else:
            while True:
                _literal, _complex, unifier = self.query_resolve()
                if unifier == 'eq':
                    result = self.resolve_with_eq_axiom(to_resolve=_complex, eq=_literal)
                else:
                    result = self.resolve(literal=_literal, clause=_complex, unifier=unifier)
                print('Got: {}'.format(result.string_rep()))
                self.clauses.append(result)
                print '\n'*20
                self.print_equality_axioms()
                self.print_clauses()

    def run_script(self):
        script = open(self.script_file)
        lines = script.readlines()
        for line in lines:
            if 'contradict:' in line:
                to_contra = line.replace('contradict: ','')
                self.assumption = to_contra.rstrip().lstrip()
            elif ';;eq;;' not in line.replace(' ',''):
                line = line.replace(' ', '')
                _complex, _literal, _unifier = line.split(';;')
                _complex = int(_complex) if str(_complex)[0].isdigit() else _complex
                _literal = int(_literal) if str(_literal)[0].isdigit() else _literal
                result = self.resolve(literal=_literal, clause=_complex, unifier=Unifier(_unifier.rstrip()))
                print('Got: {}'.format(result.string_rep()))
                self.clauses.append(result)

            else:

                _complex, x, _eq = line.rstrip().split(';;')
                _complex = int(_complex) if _complex[0].isdigit() else _complex
                _eq = int(_eq)
                result = self.resolve_with_eq_axiom(to_resolve=_complex, eq=_eq)
                self.clauses.append(result)


            # time.sleep(2)
            print '\n'*20
            self.print_equality_axioms()
            self.print_clauses()
            print('\n-------------------\nTrying to contradict:\n{}'.format(self.assumption))






class Unifier(object):
    def __init__(self, string_rep):
        self.string_rep = string_rep
        self.unifier_dict = self.convert_to_dict(string_rep)

    def convert_to_dict(self, string_rep):
        d = dict()
        # if empty unifier
        if string_rep == '{}':
            return d
        pairs_s = string_rep.replace(' ','').replace('/', ':').replace('|',':').replace('{','').\
                replace('}','').replace(' ','').split(',')
        for pair in pairs_s:
            d[pair.split(':')[0]] = pair.split(':')[1]
        return d




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--kb', default='./kb.txt')
    parser.add_argument('-s', '--script', default='./prove_script.txt')

    args = parser.parse_args()
    if args.script != "no":
        prover = ResolutionProver(args.kb, args.script)
        prover.main(from_script=True)
    else:
        prover = ResolutionProver(args.kb)
        prover.main()
    # unifier = Unifier("{t/0, s/S0}")
    # literal = "Time(0,S0)"
    # resolved = prover.resolve(literal=literal, clause=1, unifier=unifier)
    # print(resolved.string_rep())

