
# represents a Clause in a CNF sentence. ('term' could be a wrong term .... )
from predicate import Predicate
from term import Function
class Clause(object):
    def __init__(self, terms):
        self.terms = terms

    # returns a resolved clause, (Resolve literal with self, unifier=unifier)
    def resolve(self, literal, unifier):

        new_clause = Clause(terms=self.terms)
        # replace vars in values ...
        for var in unifier.unifier_dict:
            val = unifier.unifier_dict[var]
            new_clause.terms = new_clause.replace_var_with_const(var_string=var, const_string=val)
        # now remove the resolved literal
        for term in new_clause.terms:
            string_rep = term.string_rep()
            # if opposite values
            if term.value is not literal.value:
                literal_string_rep = literal.string_rep().replace('not ', '')
                term_string_rep = term.string_rep().replace('not ', '')
                if literal_string_rep == term_string_rep:
                    new_term_list = [t for t in new_clause.terms if t is not term]
                    new_clause.terms = new_term_list
                    break
        return new_clause

    def resolve_with_eq_axiom(self, eq):
        new_clause = Clause(terms=self.terms)
        new_clause.terms = self.apply_equality(eq)
        return new_clause

    def apply_equality(self, eq):
        # in case of sending eq incorrectly
        # if isinstance(eq, Clause)
        #     eq = eq.terms[0]
        new_terms = []
        for term in self.terms:
            if isinstance(eq, Predicate) and eq.value == (not term.value) and eq.string_rep().replace('not ','') ==\
                                                term.string_rep().replace('not ',''):
                # new_terms.remove(term)
                continue
            else:
                term = term.apply_equality(eq)
                new_terms.append(term)
        return new_terms


    def string_rep(self):
        s = ''
        for term in self.terms:
            s += term.string_rep()
            if not (self.terms.index(term) == len(self.terms) - 1):
                s += ' or '
        return s






    def replace_var_with_const(self, var_string, const_string):
        new_terms = []
        for term in self.terms:
            if isinstance(term, Predicate):
                term = term.replace_var(var_string,const_string)
            elif isinstance(term, Function):
                term = term.replace_var(var_string,const_string)
            new_terms.append(term)
        return new_terms



class CNF(object):
    def __init__(self, string_rep, clauses):
        self.string_rep = string_rep
        self.clauses = clauses
