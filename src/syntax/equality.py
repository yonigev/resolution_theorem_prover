
# defines an Equality between 2 terms


class Equality(object):
    def __init__(self, lhs, rhs, string_rep):
        self.rhs = rhs.rstrip()
        self.lhs = lhs
        self._string_rep = string_rep.rstrip()

    def string_rep(self):
        return self._string_rep


class Leq(Equality):
    def __init__(self, lhs, rhs, string_rep):
        super(Leq, self).__init__(lhs, rhs, string_rep)
