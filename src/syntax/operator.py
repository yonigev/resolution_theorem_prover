

class Operator(object):

    def __init__(self, string):
        self.string = string



# A ~ (NOT) operator
class Not(Operator):
    def __init__(self):
        s = '~'
        super(Not, self).__init__(string=s)



# A = (EQUAL) operator
class Equal(Operator):
    def __init__(self):
        s = '='
        super(Equal, self).__init__(string=s)


# AND  operator
class And(Operator):
    def __init__(self):
        s = 'AND'
        super(And, self).__init__(string=s)

# OR operator
class Or(Operator):
    def __init__(self):
        s = 'OR'
        super(Or, self).__init__(string=s)

