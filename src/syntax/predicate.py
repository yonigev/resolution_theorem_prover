from term import Variable, Constant, Function
class Predicate(object):
    def __init__(self, name, args, value=True):
        self.name = name
        self.args = args
        self.value = value

    def string_rep(self):
        s = '{}('.format(self.name)
        for arg in self.args:
            s += arg.string_rep()
            s += ','
        s = s[:-1]
        s += ')'
        return s if self.value is True else "not " + s



    # replace occurences of variable 'var_string' with a Constant of 'val_string'
    def replace_var(self, var_string, val_string):
        new_args = list(self.args)
        for arg in new_args:
            if arg.string_rep() == var_string and isinstance(arg, Variable):
                index = new_args.index(arg)
                new_args[index] = Constant(val_string)
            elif isinstance(arg, Function):
                index = new_args.index(arg)
                new_args[index] = arg.replace_var(var_string, val_string)
            elif isinstance(arg, Predicate):
                index = new_args.index(arg)
                new_args[index] = arg.replace_var(var_string, val_string)

        return Predicate(name=self.name, args=new_args, value=self.value)

    def apply_equality(self, eq):

        new_args = list(self.args)
        for arg in new_args:
            if isinstance(arg, Function) and arg.string_rep() == str(eq.rhs):
                index = new_args.index(arg)
                new_args[index] = Constant(str(eq.lhs))
        return Predicate(name=self.name, args=new_args, value=self.value)


