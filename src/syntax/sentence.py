
class Sentence(object):
    def __init__(self, atomic_sentence=None, complex_sentence=None):
        if atomic_sentence is not None and complex_sentence is not None:
            raise BaseException('Sentence cannot be both atomic and complex!')

        self.sentence = atomic_sentence if atomic_sentence is not None else complex_sentence


class AtomicSentence(Sentence):
    def __init__(self, predicate=None, term=None):
        if predicate is not None and term is not None:
            raise BaseException('Atomic sentence cannot be both predicate and term!')
        sentence = predicate if predicate is not None else term
        super(AtomicSentence, self).__init__(atomic_sentence=sentence)


class ComplexSentence(Sentence):
    def __init__(self, sentence):
        super(ComplexSentence, self).__init__(complex_sentence=sentence)


