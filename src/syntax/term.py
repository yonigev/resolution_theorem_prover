
class Term(object):

    def __init__(self, string):
        self._string = string

    def string_rep(self):
        return self._string


# Function(Term, Term, .... )
class Function(Term):

    def __init__(self, name, params, result=None):
        self.name = name
        self.params = self.parse_params(params_string=params)
        self.result = result
        string_rep = self.string_rep()
        super(Function, self).__init__(string=string_rep)

    def parse_params(self, params_string):
        params = []
        for param in params_string:
            if str(param[0]).isupper():
                params.append(Constant(s=param))
            elif str(param[0]).islower():
                params.append(Variable(s=param))
            else:
                params.append(Constant(s=param))
        return params




    def string_rep(self):
        s = '{}('.format(self.name)
        for arg in self.params:
            s += arg.string_rep()
            s += ','
        s = s[:-1]
        s += ')'
        return s

    def replace_var(self, var_string, val_string):
        new_params = list(self.params)
        for arg in new_params:
            if arg.string_rep() == var_string and isinstance(arg, Variable):
                index = new_params.index(arg)
                new_params[index] = Constant(val_string)
        return Function(name=self.name, params=[p.string_rep() for p in new_params], result=self.result)

    def resolve_with_eq(self, equalities):
        for eq in equalities:
            if self.string_rep() == eq.string_rep().split('=')[1]:
                return Constant(eq.lhs)

    def apply_equality(self, eq):
        if self.string_rep() == eq.rhs:
            return Constant(eq.lhs)
        return Function(name=str(self.name), params=list(self.params))



# Constant (A, X, John ... )
class Constant(Term):
    def __init__(self, s, value=None):
        super(Constant, self).__init__(string=s)
        self.value = value

    def string_rep(self):
        return self._string


# Variable (a, x, s, john ... )
class Variable(Term):
    def __init__(self, s):
        if s.lower() != s:
            raise BaseException('Variable cannot have Capital letters!')
        super(Variable, self).__init__(string=s)

    def string_rep(self):
        return self._string